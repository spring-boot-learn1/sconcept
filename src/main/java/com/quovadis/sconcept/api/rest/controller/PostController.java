package com.quovadis.sconcept.api.rest.controller;

import com.quovadis.sconcept.api.rest.dto.PostResponseDto;
import com.quovadis.sconcept.business.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/posts")
public class PostController {

    @Autowired
    PostService postService;

    @GetMapping
    public List<PostResponseDto> getPostAll(){

        return postService.searchAllPost();
    }
}
