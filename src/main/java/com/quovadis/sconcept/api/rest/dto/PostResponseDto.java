package com.quovadis.sconcept.api.rest.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data @Getter @Setter @NoArgsConstructor
public class PostResponseDto {

    private Integer id;

    private Integer userId;

    private String title;

    private String body;
}
