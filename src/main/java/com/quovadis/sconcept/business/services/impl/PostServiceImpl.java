package com.quovadis.sconcept.business.services.impl;

import com.quovadis.sconcept.api.rest.dto.PostResponseDto;
import com.quovadis.sconcept.business.domain.Post;
import com.quovadis.sconcept.business.services.PostService;
import com.quovadis.sconcept.data_access.connectors.JSONPlaceHolderConnector;
import com.quovadis.sconcept.data_access.repositories.PostRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private JSONPlaceHolderConnector postConnector;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<PostResponseDto> searchAllPost() {

        List<Post> posts = postRepository.findAll();

        if(posts.isEmpty()){
            posts = postConnector.getAll();
            postRepository.saveAll(posts);
        }

       return posts.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private PostResponseDto convertToDto(Post post) {
        PostResponseDto postDto = modelMapper.map(post, PostResponseDto.class);
        return postDto;
    }
}
