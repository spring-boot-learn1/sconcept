package com.quovadis.sconcept.business.services;

import com.quovadis.sconcept.api.rest.dto.PostResponseDto;

import java.util.List;

public interface PostService {

    List<PostResponseDto> searchAllPost();
}
