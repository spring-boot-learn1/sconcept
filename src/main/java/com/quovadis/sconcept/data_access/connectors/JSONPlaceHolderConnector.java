package com.quovadis.sconcept.data_access.connectors;

import com.quovadis.sconcept.business.domain.Post;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "jplaceholder", url="https://jsonplaceholder.typicode.com/")
public interface JSONPlaceHolderConnector {

    @RequestMapping(method = RequestMethod.GET, value = "/posts")
    List<Post> getAll();

}
