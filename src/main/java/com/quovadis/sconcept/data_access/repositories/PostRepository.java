package com.quovadis.sconcept.data_access.repositories;

import com.quovadis.sconcept.business.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
