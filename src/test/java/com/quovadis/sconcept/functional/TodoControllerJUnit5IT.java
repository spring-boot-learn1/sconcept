package com.quovadis.sconcept.functional;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.quovadis.sconcept.business.domain.Post;
import com.quovadis.sconcept.data_access.connectors.JSONPlaceHolderConnector;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {WireMockInitializer.class})
public class TodoControllerJUnit5IT {

    @Autowired
    JSONPlaceHolderConnector jsonPlaceHolderConnector;

    @Autowired
    private WireMockServer wireMockServer;

    @AfterEach
    void resetAll() {
        wireMockServer.resetAll();
    }

    @Test
    void basicWireMockExample() throws IOException {

        System.out.println("Wiremock URL: " + WireMock.urlEqualTo("/api/v1/posts"));
        wireMockServer.stubFor(
                WireMock.get(WireMock.urlEqualTo("/api/v1/posts"))
                //WireMock.get("https://jsonplaceholder.typicode.com/posts")
                        .willReturn(aResponse()
                                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                                .withBodyFile("jsonplaceholder-api/response-200.json"))
        );

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(String.format("http://localhost:%s/api/v1/posts", wireMockServer.port()));
        //HttpGet request = new HttpGet("https://jsonplaceholder.typicode.com/posts");
        HttpResponse httpResponse = httpClient.execute(request);
        String stringResponse = convertHttpResponseToString(httpResponse);


        System.out.println("Test");
    }

    private static String convertHttpResponseToString(HttpResponse httpResponse) throws IOException {
        InputStream inputStream = httpResponse.getEntity().getContent();
        return convertInputStreamToString(inputStream);
    }

    private static String convertInputStreamToString(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        String string = scanner.useDelimiter("\\Z").next();
        scanner.close();
        return string;
    }
}
